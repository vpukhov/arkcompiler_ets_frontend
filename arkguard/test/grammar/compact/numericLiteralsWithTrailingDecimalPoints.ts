/*
 * Copyright (c) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

1..toString();
1.0.toString();
1. + 2.0 + 3.;

var i: number = 1;
var case1 = i.toString();
var case3 = 3 .toString();
var case4 = 3    .toString();
var case5 = 3	.toString();
var case6 = 3.['toString']();
var case7 = 3
  .toString();
var case8 = new Number(4).toString();
var case9 = 3. + 3.;
var case10 = 0 /* comment */.toString();
var case11 = 3. /* comment */.toString();
var case12 = 3
  /* comment */ .toString();
var case122 = 3
/* comment */.toString();
var case1222 = 3

  .toString();
var case13 = 3.
  /* comment */.toString();
var case14 = 3
  // comment
  .toString();
var case15 = 3.
  // comment
  .toString();
var case16 = 3  // comment time
  .toString();
var case17 = 3. // comment time again
  .toString();




